<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    public function welcome()
    {
        $posts = Post::inRandomOrder()->limit(3)->get();

        return view('welcome')->with('posts', $posts);
    }


    public function create()
    {
        return view('posts.create');
    }


    public function store(Request $request) 
    {   
        //if there is an authenticated user
        if(Auth::user()){
            //create a new Post object from the Post model
            $post = new Post;

            $post->title = $request->input('title');
            $post->content =  $request->input('content');
            //get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            //save the post object to the database
            $post->save();

            return redirect('/posts');

        }else {
                return redirect('/login');
        }
    }


    public function index()
    {   

        //get all posts from the database

        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }


    public function myPost()
    {
        if(Auth::user())
        {
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);

        }else {
            return redirect('/login');
        }
    }


    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }


    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

}
