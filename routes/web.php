<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'welcome']);


Auth::routes();

Route::get('/home', [HomeController::class, 'index']);

// route to return a view where user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

//route for a route wherein form data can be sent via POST method
Route::post('/posts', [PostController::class, 'store']);

//route that will return a view contaiing all post
Route::get('/posts', [PostController::class, 'index']);

Route::get('/posts/myPost', [PostController::class, 'myPost']);

//route that will show specific posts' view based on the url parametesrs id

Route::get('/posts/{id}', [PostController::class, 'show']);


Route::get('/posts/{id}/edit', [PostController::class, 'edit']);